# ATtiny84 Low Power LoRa Node
This code is written for a ATtiny84 with a RFM95W module and BME280 environment sensor.
Due to the flash size (8k) a proprietary library for LoRaWAN is used.
Rewritten in OOP (Object Oriented Programming) style with classes for RFM95 and LoRaWAN.
The low power options of the ATtiny84 are used.
It can only use ABP activation and no downlink messages can be received.
Only Temperature and Humidity can be read with this code. The Pressure code does not fit in this controller.
Tested and used with The Things Network in Europe.

## More information
See https://www.iot-lab.org/blog/101/ for more information on the hardware and setup.

## Development environment
This code was written and build with the PlatformIO tools.
Running on Mac OS and using the Atom editor.

## 2021-04-07 changes
Support for The Things Stack V3 added. See https://www.iot-lab.org/blog/663/ for more information on settings on The Things Stack Community for the use of this code and read the comments in the main program.

## 2020-08-15 changes
- Added this README File
- the code can be compiled (again) with the latest Arduino core
- due to changes in the ```framework-arduino-avr-attiny/cores/tiny/Arduino.h```
the pin numbering was not correct anymore. Added definitions for the original pin numbering of the ATtiny84 chip.
